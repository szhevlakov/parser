<?php
namespace GoogleParser;

/**
 * Class Parser
 * @package GoogleParser
 */
class Parser
{
    /**
     *
     */
    const KEYS_FILE = 'keys.txt';
    const USER_AGENT = 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0';
    const OBJ_DOMAIN = 'php.net';
    const PAGE_LIMIT = 10;
    const DEFAULT_LOG_FILE = __DIR__ . '/../logs/error.log';

    protected $curl;
    protected $query;
    protected $pages = 1;
    protected $start = 0;
    protected $search_url = "https://www.google.com.ua/search?q=[QUERY]&start=[START]";
    protected $db;

    public function __construct()
    {
        $this->curl = curl_init();
        $proxy = '176.100.20.98:8080';
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->curl, CURLOPT_HTTPPROXYTUNNEL, 1);
        curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($this->curl, CURLOPT_PROXY, $proxy);
    }

    /**
     *
     */
    public function parse()
    {
        $keys_array = $this->getKeys();

        if (!empty($keys_array)) {
            foreach ($keys_array as $index => $query_key) {
                for ($i = 0; $i <= $this->pages; $i++) {
                    $this->query = urlencode(trim($query_key));
                    $this->start = $this->getStartPage($i);
                    $url = str_replace(['[QUERY]', '[START]'], [$this->query, $this->start], $this->search_url);

                    curl_setopt($this->curl, CURLOPT_URL, $url);
                    $result = curl_exec($this->curl);
                    $code_result = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);

                    if ($code_result != '200') {
                        $this->writeErrorLog('Something goes wrong, check search url!');
                        exit;
                    }
                    $matches = [];
                    preg_match_all('|<h3 class="r">.*?href="/url\?q=(.*?)&amp;.*?".*?</h3>|', $result, $matches);

                    if(isset($matches[1]) && count($matches[1]) > 0){
                        $this->saveData($matches[1]);
                    }
                }
            }
        }
    }

    /**
     * @param $data
     * @throws \Exception
     */
    public function saveData($data)
    {

        $this->db = new \Database();
        foreach($data as $key => $item){

            $today_exists = $this->db->getRows("SELECT id, FROM_UNIXTIME(`created_at`, '%Y-%m-%d') `date` FROM search WHERE `key` = ? and `url` = ? Order By `date` DESC",
                                             [trim($this->query), trim($item)]);

            if(count($today_exists) > 0 && $today_exists[0]['date'] != date('Y-m-d', time())){
                $parse = parse_url($item);
                $bunch = [
                    $item,
                    $this->query,
                    $key+1,
                    $parse['host'],
                    time()
                ];
                $this->db->insertRow("INSERT INTO search(`url`, `key`, `position`, `domain`, `created_at`)
                          VALUES (?, ?, ?, ?, ?)", $bunch);
            }
        }
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getReport()
    {
        $report_data = [];

        $this->db = new \Database();
        $keys = $this->getKeys();

        foreach($keys as $key){
            $report_data[$key] = $this->db->getRows("SELECT `url`, `key`, `position`, `domain`, FROM_UNIXTIME(`created_at`, '%Y-%m-%d') `date`
                                                      FROM search WHERE `key` = ? Order By `date` DESC", array(trim($key)));
        }
        $group_by_date = [];
        if(count($report_data) > 0){
            foreach($report_data as $key => $val){
                foreach($val as $item){
                    $group_by_date[$key][$item['date']][] = $item;
                }
            }
        }

        return $this->getFormattedDataForReport($group_by_date);
    }

    /**
     * @param $group_by_date
     * @return array
     */
    public function getFormattedDataForReport($group_by_date)
    {
        $formatted_data = [];
        foreach($group_by_date as $key => $date_data){
            foreach($date_data as $date => $date_block){
                foreach($date_block as $row){
                    $yesterday = date('Y-m-d', strtotime($date .' -1 day'));

                    if(isset($group_by_date[$key][$yesterday])){
                        foreach($group_by_date[$key][$yesterday] as $prev_data){
                            if($row['key'] == $prev_data['key'] && $row['domain'] == $prev_data['domain']){
                                $formatted_data[$key][$date][] = [
                                    'url' => $row['url'],
                                    'key' => $row['key'],
                                    'position' => $row['position'],
                                    'domain' => $row['domain'],
                                    'date' => $row['date'],
                                    'position_is_changed' => ($row['position'] != $prev_data['position'])
                                        ? ($row['position'] > $prev_data['position']) ? 1 : 2
                                        : 0,
                                    'page_is_changed' => ($row['url'] != $prev_data['url']) ? 1 : 0,
                                    'is_objective_domain' => $row['domain'] == self::OBJ_DOMAIN ? 1 : 0
                                ];
                            }
                        }
                    }else{
                        $row['position_is_changed'] = 0;
                        $row['page_is_changed'] = 0;
                        $row['is_objective_domain'] = $row['domain'] == self::OBJ_DOMAIN ? 1 : 0;
                        $formatted_data[$key][$date][] = $row;
                    }
                }
            }
        }

        return count($formatted_data) > 0 ? $formatted_data : $group_by_date;
    }

    /**
     * @return array|bool
     */
    public function getKeys()
    {
        $keys_file = @file_get_contents(self::KEYS_FILE);
        if ($keys_file !== false) {
            $rows = explode("\n", $keys_file);

            if (empty($rows[0])) {
                $this->writeErrorLog('Keys file is empty!');
                return false;
            }
            return $rows;
        }
        $this->writeErrorLog('File does not exists!');
        return false;
    }


    /**
     * @param $page
     */
    public function getStartPage($page)
    {
        return $page * self::PAGE_LIMIT;
    }

    /**
     * @param $message
     * @param bool|false $file
     * @return bool
     */
    public function writeErrorLog($message, $file = false)
    {
        if (empty($message)) {
            return false;
        }
        $logfile = (self::DEFAULT_LOG_FILE && !$file) ? self::DEFAULT_LOG_FILE : $file;

        $date = date("Y-m-d H:i:s", time());
        $fd = fopen($logfile, (file_exists($logfile)) ? 'a' : 'w');
        $script_name = pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME);
        $result = fwrite($fd, "$date ($script_name) $message" . PHP_EOL);
        fclose($fd);
        if ($result) {
            return true;
        } else {
            exit('Unable to write to ' . $logfile . '!');
        }
    }

    public function __destruct()
    {
        curl_close($this->curl);
    }

}