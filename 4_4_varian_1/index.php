<?php
require('config/db.php');
require('classes/DB.php');
require('classes/Parser.php');

$parseReport = new \GoogleParser\Parser();

$report_data = $parseReport->getReport();
echo mktime(0, 0, 0, date("m"), date("d")-1, date("Y"));die;
?>


<!DOCTYPE html>
<html>
<head>
    <title>Report:<?= $parseReport::OBJ_DOMAIN; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Bootstrap core CSS -->
    <link href="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet"
          media="screen">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
    <![endif]-->
    <style>
       .obj_domain{
           font-weight: bold;
       }
    </style>
</head>
<body>
<h1>Report: <?= $parseReport::OBJ_DOMAIN; ?></h1>

<div>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Дата</th>
                <th>Запрос</th>
                <th>Домен</th>
                <th>Страница</th>
                <th>Позиция</th>
                <th>Изменилась ли страница</th>
            </tr>
        </thead>
        <?php if (count($report_data) > 0): ?>
            <?php foreach ($report_data as $key => $date_block): ?>
                <?php foreach ($date_block as $date => $date_group): ?>
                    <?php foreach ($date_group as $row): ?>
                       <tr <?= ($row['position_is_changed'] != 0)
                           ? ($row['position_is_changed'] == 2 ? 'class="danger"' : 'class="success"')
                           : '';
                       ?>>
                           <td <?= $row['is_objective_domain'] ? 'class="obj_domain"' : ''; ?>><?= $row['date']; ?></td>
                           <td <?= $row['is_objective_domain'] ? 'class="obj_domain"' : ''; ?>><?= $row['key']; ?></td>
                           <td <?= $row['is_objective_domain'] ? 'class="obj_domain"' : ''; ?>><?= $row['domain']; ?></td>
                           <td <?= $row['is_objective_domain'] ? 'class="obj_domain"' : ''; ?>><?= $row['url']; ?></td>
                           <td <?= $row['is_objective_domain'] ? 'class="obj_domain"' : ''; ?>><?= $row['position']; ?></td>
                           <td <?= $row['is_objective_domain'] ? 'class="obj_domain"' : ''; ?>><?= $row['page_is_changed'] ? 'Да' : 'Нет'; ?></td>
                       </tr>
                    <?php endforeach; ?>
                <?php endforeach; ?>
            <?php endforeach; ?>
        <?php endif; ?>
    </table>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.4/js/bootstrap.min.js"></script>
</body>
</html>

