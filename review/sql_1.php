<?php
/*
 * опишите что этот код делает и можно ли его улучшить
 * заметьте - это Postgresql - предположительно не знакомая вам база данных и вам нужно почитать документацию прежде чем ответить
 */


for($i = 0; $i<10000000; $i++) {
	$result = DB::query('
		CREATE TEMPORARY TABLE tmp1 AS SELECT u.id
			FROM public.users AS u
			INNER JOIN public.user_requests AS o ON (o.id_user = u.id)
			WHERE (u.mark IS NULL OR u.mark<4)
        	AND o.status IN (1,2) LIMIT 100');
	$result_count = DB::query('SELECT COUNT(*) INTO return_count FROM tmp1', 'one');
	$result = DB::query('
		UPDATE public.users SET mark= 4
    		FROM (SELECT id FROM tmp1) AS subquery
    		WHERE public.users.id = subquery.id');
	$result = DB::query('DROP TABLE tmp1');
	if ($result_count < 100) break;
	//echo $i . ' ';
}