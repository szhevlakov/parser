<?php
/*
 * опишите что этот код делает и найдите ошибки если они есть
 */
$start_time = microtime(true);

require_once "app/libs/_settings.php";

define('LIST_TITLE', 'Интернет магазин');
define('LIST_ID', 1233);
define('SHOW_SUBCATS', false);
define('SHOW_HOTLINE_CATS', true);
define('SHOW_ID_PLUS', 10000);

$products = DB::query('SELECT p.id, p.title, p.hotline_title, p.hotline_art_number, p.art_number, p.friendly, p.uah_price, p.intro, p.`desc`, p.supplier_id, p.type_id, p.hotline_cat_id FROM products AS p
			WHERE p.is_deleted=0 AND p.is_exported_hotline=1  AND p.price>0 ORDER BY p.id ASC', 'alist');
if (!$products) exit;

if (SHOW_SUBCATS) {
	$products_categories = DB::query('SELECT product_id, category_id FROM categories_products 
										WHERE product_id IN (' . implode(',', array_keys($products)) . ')', 'mlist');
	$categories = DB::query('SELECT id, title, parent_id, type_id FROM categories', 'alist');
	if (!$categories) exit;
} else {
	$products_categories = $categories = array();
}

$types =  DB::query('SELECT id, title, friendly FROM types', 'alist');
if (!$types) exit;
if (SHOW_HOTLINE_CATS) {
	$hotline_cats =  DB::query('SELECT id, title FROM hotline_cats', 'list');
} else {
	$hotline_cats = array();
}

$suppliers =  DB::query('SELECT id, title FROM suppliers', 'list');
if (!$suppliers) exit;


function clearTitle($title) {
	$title = strip_tags($title);
	$bad = array('"', "'", "&", "<", ">");                      //плохие символы
	$good = array("&quot;", "&apos;", "&amp;", "&lt;", "&gt;", ""); //хорошие символы
	return str_replace($bad, $good, $title);            //меняем плохие на хорошие
}

header("Content-Type: text/xml");
header("Cache-Control: no-cache, must-revalidate");
header("Cache-Control: post-check=0,pre-check=0");
header("Cache-Control: max-age=0");
header("Pragma: no-cache");

//hotline.ua/about/delivery_method_xml/
echo '<?xml version="1.0" encoding="UTF-8"?>
<price>
	<date>' . date('Y-m-d H:i:s') . '</date>
	<firmName>' . LIST_TITLE . '</firmName>
	<firmID>' . LIST_ID . '</firmID>
	<delivery id="1" type="address" carrier="slf" cost="0" freeFrom="0" time="3" region="01*-04*" />
	<categories>
	';
	if ($hotline_cats) {
		foreach ($hotline_cats AS $cid => $type) {
			echo '<category><id>' . $cid . '</id><name>' . clearTitle($type) . '</name></category>' . PHP_EOL;
		}
	} else {
		foreach ($types AS $cid => $type) {
			echo '<category><id>' . $cid . '</id><name>' . clearTitle($type['title']) . '</name></category>' . PHP_EOL;
		}
	}
	if ($categories) {
		foreach ($categories AS $cid => $category) {
			$cid = $cid + SHOW_ID_PLUS;
			$pid = $category['parent_id'] ? ($category['parent_id'] + SHOW_ID_PLUS) : $category['type_id'];
			echo '<category><id>' . $cid . '</id><name>' . clearTitle($category['title']) . '</name><parentId>' . $pid . '</parentId></category>' . PHP_EOL;
		}
	}
	echo '</categories>' . PHP_EOL;
	echo '<items>' . PHP_EOL;


	foreach ($products AS $pid => $product) {
		$supplier_title     = $suppliers[$product['supplier_id']];
		if ($supplier_title == 'Техностиль Про') {
			$supplier_title = 'Office4You';
		}
		$search_title = mb_strtolower($product['title'], 'utf-8');
		$cat_id = $hotline_cats ? $product['hotline_cat_id'] : $product['type_id'];
		if (!$cat_id) $cat_id = 16;
		$desc = clearTitle($product['intro'] ? $product['intro'] : $product['desc']);
		$desc_short = mb_substr($desc, 0, 145, 'utf-8');
		$desc_last_3 =  mb_substr($desc, 145, 5, 'utf-8');
		if (!(strpos($desc_last_3, '&') === false)) {
			$tmps = explode('&', $desc_last_3);
			$desc_short .= $tmps[0];
		}
		echo '<item>
			<id>' . $pid . '</id>
			<categoryId>' . $cat_id . '</categoryId>
			<code>' . clearTitle($product['hotline_art_number'] ? $product['hotline_art_number'] : $product['art_number']) . '</code>
			<vendor>' . $supplier_title . '</vendor>
			<name>' . clearTitle($product['hotline_title'] ? $product['hotline_title'] : $product['title']) . '</name>
			<description>' . $desc . '</description>
			<url>' . URL . 'kupit-' . $types[$product['type_id']]['friendly'] . '/' . $product['friendly'] . '.html</url>
			<image>' . F::image($product['friendly'], $product['id'], '188-122-boxed', 'product') . '</image>
			<priceRUAH>' . clearTitle($product['uah_price']) . '</priceRUAH>
			<stock>На складе</stock>
			<guarantee>12</guarantee>' . PHP_EOL;
		if ($products_categories && isset($products_categories[$pid])) {
			foreach ($products_categories[$pid] AS $cid) {
				echo '<categoryId>' . ($cid + SHOW_ID_PLUS) . '</categoryId>';
			}
		}
		echo '</item>';
	}
	echo '</items>' . PHP_EOL;
echo '</price>';
exit;